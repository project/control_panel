<?php

namespace Drupal\control_panel\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Dashboard routes.
 */
class ControlPanelController extends ControllerBase {

  /**
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $content = [];
    $content['#markup'] = '';
    $content['#prefix'] = '<div class="control-panel">';
    $content['#suffix'] = '</div>';
    $content['#attached']['library'][] = 'control_panel/control_panel.grid';

    $views = [];

    $config = \Drupal::config('control_panel.settings')->get('embedded_views');
    $separator = PHP_EOL;
    $line = strtok($config, $separator);

    while ($line !== false) {
      $view = explode(':', $line);
      $views[] = views_embed_view(trim($view[0]), trim($view[1]));
      $line = strtok($separator);
    }

    foreach ($views as $view) {
      $content['#markup'] .= $this->renderer->render($view);
    }

    return $content;
  }

}
