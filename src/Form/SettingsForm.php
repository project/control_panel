<?php

namespace Drupal\control_panel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Control Panel settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'control_panel_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['control_panel.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['embedded_views'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Control Panel Views'),
      '#description' => $this->t('Add Views Embed displays in the form of "view_machine_name:display_machine_name". One entry per line!'),
      '#default_value' => $this->config('control_panel.settings')->get('embedded_views'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * TODO: Add validation.
   */
  // public function validateForm(array &$form, FormStateInterface $form_state) {
  //   if ($form_state->getValue('example') != 'example') {
  //     $form_state->setErrorByName('example', $this->t('The value is not correct.'));
  //   }
  //   parent::validateForm($form, $form_state);
  // }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('control_panel.settings')
      ->set('embedded_views', $form_state->getValue('embedded_views'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
