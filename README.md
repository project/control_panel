# Control Panel

Creates a toolbar item named "Control Panel" which can have subitems.

## Views
To add a subitem for a View, from the Views UI interface select:
```
Menu -> Normal menu entry -> Parent -> Control Panel
```
Title, description and weight can be filled in as needed.

## Custom Links
To link to any route create a custom module and under `yourmodule.links.menu.yml` and add an entry like:
```
control_panel.example:
  parent: control_panel.admin
  route_name: node.add
  route_parameters:
    node_type: 'fqt_article'
  title: 'Add FAQ Article'
  description: 'Create new content'

control_panel.example2:
  parent: control_panel.admin
  route_name: view.feedback.nodes_list
  title: 'Manage Feedback'
  description: 'See and manage all feedback submitted to your site'
```
